import { View, Text, SafeAreaView, TouchableOpacity } from "react-native";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "./counterSlice";

export default function Counter() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  return (
    <SafeAreaView>
      <View>
        <TouchableOpacity
          onPress={() => {
            dispatch(increment());
          }}
        >
          <Text>Increment</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text>{count}</Text>
      </View>
      <View>
        <TouchableOpacity
          onPress={() => {
            dispatch(decrement());
          }}
        >
          <Text>Decrement</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}
